package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"time"
)

type TimeEntry struct {
	Id          int       `json:"id"`
	Guid        string    `json:"guid"`
	Wid         int       `json:"wid"`
	Billable    bool      `json:"billable"`
	Start       time.Time `json:"start"`
	Stop        time.Time `json:"stop"`
	Duration    int       `json:"duration"`
	Tags        []string  `json:"tags"`
	Duronly     bool      `json:"duronly"`
	At          string    `json:"at"`
	Uid         int       `json:"uid"`
	Description string    `json:"description"`
}

type DayEntry struct {
	Duration int
	Date     time.Time
}

func main() {
	var excelOutput string

	dateStart, dateEnd, err := parseArgs()
	if err != nil {
		log.Fatal(err)
	}
	jsonResponse := getApiResponse(dateStart, dateEnd)

	days := make(map[string]DayEntry)
	var keys []string
	for date := dateStart; date.Before(dateEnd); date = date.AddDate(0,0,1) {
		i:= date.Format("2006-01-02")
		days[i] = DayEntry{
			Duration: 0,
			Date:     date,
		}
		keys = append(keys, i)
	}

	for _, timeEntry := range jsonResponse {
		if timeEntry.Duration < 0 {
			continue
		}
		day := timeEntry.Start.Format("2006-01-02")
		currentDuration := days[day]
		days[day] = DayEntry{
			Duration: currentDuration.Duration + timeEntry.Duration,
			Date:     timeEntry.Start,
		}
	}

	// sort.Strings(keys)

	fmt.Println("Details:")
	for _, k := range keys {
		dayEntry := days[k]
		workedMinutes := dayEntry.Duration / 60
		diff := int(math.Round(float64(dayEntry.Duration-8*60*60) / 60))
		if !dayEntry.Date.IsZero() {
			fmt.Printf("%s, Worked: %d minutes. diff: %d\n",dayEntry.Date.Format("2006-01-02 (Monday)"),workedMinutes,diff)
		}

		var diffStr string
		if dayEntry.Duration == 0 {
			diffStr = "-"
		} else {
			weekday := dayEntry.Date.Weekday()
			if weekday != 0 && weekday != 6 {
				diffStr = strconv.Itoa(int(math.Round(float64(dayEntry.Duration-8*60*60) / 60)))
			} else {
				diffStr = strconv.Itoa(dayEntry.Duration / 60)
			}
		}

		excelOutput += diffStr + "\t"
	}

	fmt.Println("\nExcel output:")
	fmt.Println(excelOutput)
}

func parseArgs() (time.Time, time.Time, error) {
	var dateEnd time.Time
	var dateStart time.Time
	args := os.Args
	if len(args) == 2 {
		var err error
		dateStart, err = time.Parse("2006-01-02", args[1])
		if err != nil {
			return time.Time{}, time.Time{}, errors.New("invalid date format. use: YYYY-MM-DD")
		}
	} else {
		dateStart = time.Now().AddDate(0,0,-5)
	}
	dateEnd = dateStart.AddDate(0,0, 5)

	return dateStart, dateEnd, nil
}

func getApiResponse(dateStart time.Time, dateEnd time.Time) []TimeEntry {
	client := http.Client{}

	url := "https://www.toggl.com/api/v8/time_entries?start_date=" + dateStart.Format("2006-01-02") + "T00%3A42%3A46%2B02%3A00&end_date=" + dateEnd.Format("2006-01-02") + "T00%3A42%3A46%2B02%3A00"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Content-type", "applicaton/json")
	apiToken := os.Getenv("TOGGL_API_TOKEN")
	if apiToken == "" {
		log.Fatal("Missing TOGGL_API_TOKEN env")
	}
	req.SetBasicAuth(apiToken, "api_token")

	res, getErr := client.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)

	if readErr != nil {
		log.Fatal(readErr)
	}

	var jsonResponse []TimeEntry

	unmarshalErr := json.Unmarshal(body, &jsonResponse)
	if unmarshalErr != nil {
		str := string(body)
		fmt.Println(str)
		log.Fatal(unmarshalErr)
	}
	return jsonResponse
}
