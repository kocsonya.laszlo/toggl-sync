It reads the next 5 days (starting from startDate) from toggl and outputs the diff minutes in a format that can be copy + pasted into the google sheet.

## Usage 
```
TOGGL_API_TOKEN=xxxx ./toggl-sync {startDate}
```

## Example
```
$ ./build/toggl-sync 2020-05-11
Details:
2020-05-11 (Monday) , Worked: 455 minutes diff: -24
2020-05-12 (Tuesday) , Worked: 453 minutes diff: -26
2020-05-13 (Wednesday) , Worked: 566 minutes diff: 86
2020-05-14 (Thursday) , Worked: 469 minutes diff: -10
2020-05-15 (Friday) , Worked: 468 minutes diff: -11

Excel output:
-24	-26	86	-10	-11	
```
